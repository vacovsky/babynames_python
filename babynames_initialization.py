from datetime import datetime
import data_access

ENABLE_PRECACHE = True

if ENABLE_PRECACHE:
    genders = ['M', 'F']
    locales = ['US', 'AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA', 'HI', 'ID',
               'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO',
               'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR', 'PA',
               'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY']

    print('Beginning pre-cache process - this dramatically improves search times for users when searching by region.')
    pcstart = datetime.now()
    for g in genders:
        for l in locales:
            start = datetime.now()
            data_access.get_name_data_in_region('Joe', g, l, first_year=1880)
            print(g, l, (datetime.now() - start).total_seconds())

    print('Pre-caching complete! Total time (seconds):',
          (datetime.now() - pcstart).total_seconds())
