from flask import Flask, render_template, request, jsonify
import setproctitle
import datetime
import time
import data_access

__author__ = 'joe vacovsky jr'
setproctitle.setproctitle("BabyNames")
app = Flask(__name__)


def get_id():
    now = datetime.datetime.now()
    return time.mktime(now.timetuple())


@app.route('/BabyNames/NameData', methods=['GET'])
def name_data():
    started = datetime.datetime.now()
    name = ""
    gender = 'M'
    year = 1950
    all_args = request.args.to_dict()

    if "name" in all_args:
        name = all_args["name"]

    if "gender" in all_args:
        gender = all_args["gender"]

    if "year" in all_args:
        year = int(all_args["year"])

    if "locale" in all_args:
        locale = all_args["locale"]

    results = {"Name": name,
               "Gender": gender,
               "YearData": {}
               }

    for y in range(year, 2015):
        results["YearData"][y] = 0.0

    data = data_access.get_name_data_in_region(
        name, gender, locale, first_year=year)
    for k, v in data.items():
        results["YearData"][k] = v
    ended = datetime.datetime.now()
    results["query_time"] = str(ended - started)

    return jsonify(results)


@app.route('/', methods=["GET"])
def index_loader():
    return render_template("index.html")

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=5001, threaded=True)
