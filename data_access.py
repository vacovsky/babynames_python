import sqlite3
import cachemanager
import CONFIG
from datetime import datetime


USE_SQLITE = True
if USE_SQLITE:
    DBPATH = CONFIG.DBPATH

else:
    import pymysql as MySQLdb


db_user = ''
db_pass = ''
db_server = ''
db_name = ''
db_conn = db_server, db_user, db_pass, db_name,


def runQuery(query=tuple, connstring=db_conn):
    if not USE_SQLITE:
        db = MySQLdb.connect(
            host=connstring[0],
            user=connstring[1],
            passwd=connstring[2],
            db=connstring[3])
    else:
        db = sqlite3.connect(DBPATH)
    cursor = db.cursor()
    cursor.execute(query)
    response = cursor.fetchall()
    db.close()
    return response


def get_total_gender_for_year(year, gender, locale):
    sqlStr = """SELECT SUM(Count) FROM """
    if locale.upper() == 'US':
        sqlStr += " nationalnames "
        sqlStr += " WHERE Year=%s AND Gender='%s';" % (year, gender)
    else:
        sqlStr += " statenames "
        sqlStr += " WHERE year=%s AND Gender='%s' AND State='%s';" % (
            year, gender, locale)
    result = runQuery(query=sqlStr)[0][0]
    if result is None:
        result = 0
    return(result)


def get_name_data_in_region(name, gender, locale, first_year=1950):
    year_totals = {}

    for y in range(first_year, 2015):
        cache_key = gender + ':' + locale + ':' + str(y)
        total_gender_for_year = cachemanager.get_or_set_obj(cache_key)

        if total_gender_for_year is None:
            print('year totals not found in Redis: adding them now.  You could precache this to speed up the process!')
            total_gender_for_year = int(
                get_total_gender_for_year(y, gender, locale))
            cachemanager.get_or_set_obj(cache_key, total_gender_for_year)

        year_totals[y] = int(total_gender_for_year)

    return get_name_data(name, gender, locale, year_totals, first_year)


def get_name_data(name, gender, locale, year_totals, first_year=1950):
    if locale == 'US':
        sql = """SELECT Year, Count FROM nationalnames
WHERE Year>=%s AND Gender='%s' AND Name='%s' ORDER BY
Year ASC;""" % (first_year, gender, name)

    else:
        sql = """SELECT Year, Count FROM StateNames
WHERE Year>=%s AND Gender='%s' AND Name='%s' AND State='%s' ORDER BY
Year ASC;""" % (first_year, gender, name, locale)

    data = {}
    result = runQuery(query=sql)
    for r in result:
        data[r[0]] = float("%.3f" % ((r[1] / int(year_totals[r[0]])) * 100))
    return(data)


if CONFIG.ENABLE_PRECACHE:
    if not cachemanager.get_or_set_obj('BN:PRECACHED') == 'True':

        #### Pre-populate cache on startup ####
        genders = ['M', 'F']
        locales = ['US', 'AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA',
                   'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI',
                   'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND',
                   'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA',
                   'WA', 'WV', 'WI', 'WY']

        print('Beginning pre-cache process - this dramatically improves search times for users when searching by region.')
        pcstart = datetime.now()
        for g in genders:
            for l in locales:
                start = datetime.now()
                get_name_data_in_region('Joe', g, l, first_year=1880)
                print(g, l, (datetime.now() - start).total_seconds())
        cachemanager.get_or_set_obj('BN:PRECACHED', 'True')

        print('Pre-caching complete! Total time (seconds):',
              (datetime.now() - pcstart).total_seconds())
